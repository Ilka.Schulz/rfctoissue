import sys
import requests
import string

# parse arguments
if len(sys.argv) != 2:
    raise ValueError('Please provide one arguments RFC number')
rfc_number = sys.argv[1]

# fetch raw text
url = f'https://www.rfc-editor.org/rfc/rfc{rfc_number}.txt'
raw = requests.get(url)
raw.raise_for_status()
raw = raw.content.decode('utf-8')

# remove everything before first section
raw = '1. '+raw.split('\n1. ',1)[1]
raw = raw.replace('\r\n','\n')
for i in range(20):
    raw = raw.replace('\n\n\n','\n\n')  # TODO: find sth. like replace_all


# process lines
parts = [f'# [RFC {rfc_number}]({url})']
paragraph=''
verbatim=''
for line in raw.split('\n'):
    if len(line) == 0:
        # flush paragraph
        if len(paragraph) != 0:
            parts.append(paragraph)
            paragraph=''
        # flush verbatim
        if len(verbatim) != 0:
            parts.append(verbatim)
            verbatim=''
    elif line[0] in string.digits:
        # caption
        if "References" in line:
            # skip everything after "References" caption
            break
        else:
            # flush paragraph
            if len(paragraph) != 0:
                parts.append(paragraph)
                paragraph=''
            # flush verbatim
            if len(verbatim) != 0:
                parts.append(verbatim)
                verbatim = ''
            # add markdown '###' to caption
            caption = ''
            for i in range(line.split(' ',1)[0].count('.')+1):
                caption += "#"
            caption += " "+line
            parts.append(caption)   
    elif line[0] == ' ':
        if len(paragraph)==0 and len(verbatim)==0:
            # determine text type (paragraph or verbatim)
            if len(line)>=4 and line[0:4] == '    ':
                # verbatim text
                verbatim = ' '
            else:
                # paragraph
                paragraph = ' '
        # add line to paragraph/verbatim
        if len(paragraph) != 0:
            line = line.strip(' ')
            paragraph += line+' '
        elif len(verbatim) != 0:
            if len(verbatim)==1 and verbatim[0]==' ':
                verbatim = ''
            verbatim += line+'\n'
        else:
            raise ValueError('paragraph and verbatim are both empty')

# flush paragraph
if len(paragraph) != 0:
    parts.append(paragraph)
    paragraph = ''
# flush verbatim
if len(verbatim) != 0:
    parts.append(verbatim)
    verbatim = ''

#


# print
for part in parts:
    if len(part) == 0:
        continue
    #else:
    #    print(part)
    #continue
    elif part[0] == '#':
        # print caption
        print('\n'+part)
    else:
        if len(part)>=4 and part[0:4] == '    ':
            # verbatim text
            print('```text\n'+part+'```')
        else:
            # split paragraph into sentences
            naive_sentences = part.split('. ')
            sentences = [f''];
            sentence = ''
            for naive_sentence in naive_sentences:
                naive_sentence = naive_sentence.strip(' ')
                sentence += naive_sentence+'. '
                if len(sentence) >= 5:
                    sentences.append(sentence)
                    sentence = ''
            for sentence in sentences:
                sentence = sentence.strip(' ')
                if len(sentence) == 0:
                    continue
                if len(sentence) != 0:
                    print('- [ ] '+sentence)

#print(type(raw))
